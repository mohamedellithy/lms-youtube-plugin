<?php 
 
require_once EDR_PLUGIN_DIR.'App/class/class-wp-api-get-youtube-playlist.php';
require_once EDR_PLUGIN_DIR.'App/class/admin/class-wp-add-admin-pages-playlist-settings.php';
require_once EDR_PLUGIN_DIR.'App/class/class-wp-api-get-add-playlist-video-to-post-lesson.php';

//https://www.youtube.com/watch?v=UDvh63xHVa0&list=PLoqNzfHlA__knCeUoKUHjQfZpUL6mj64w

new wp_add_admin_pages_playlist_settings();



add_action('admin_enqueue_scripts','install_scripts_playlist_youtube');
function install_scripts_playlist_youtube(){
     wp_enqueue_script('jquery');	    
     wp_enqueue_script('youtube_playlist',EDR_PLUGIN_URL.'/App/assets/js/playlist_ajax.js',array('jquery'),false,true);
     wp_localize_script('youtube_playlist','youtube_playlist_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
		   
}


add_action('wp_ajax_nopriv_get_youtube_playlist','get_youtube_playlist');

add_action('wp_ajax_get_youtube_playlist','get_youtube_playlist');

function get_youtube_playlist(){
	$add_videos_to_lessons = new wp_api_get_add_playlist_video_to_post_lesson($_POST['playlist_link'],$_POST['course_id'],$_POST['type_of_get_lessons']);
  $add_videos_to_lessons->add_lesson_for_course();
    wp_die();
}



add_action('wp_ajax_nopriv_show_video_playlist_in_table','show_video_playlist_in_table');

add_action('wp_ajax_show_video_playlist_in_table','show_video_playlist_in_table');

function show_video_playlist_in_table(){
	  $show_lessons_in_table = new wp_api_get_add_playlist_video_to_post_lesson($_POST['playlist_link']);
    $show_lessons_in_table->render_html_lessons_youtube();
     wp_die();
}



function add_key_for_playlist_api(){
      // Stop running function if form wasn't submitted
	if(isset($_POST['add_key_api_youtube'])){
      if ( !isset($_POST['playlist_key']) ) {
          return;
      }
      update_option('playlist_key_youtube',$_POST['playlist_key']);
      header('location: '.$_POST['_wp_http_referer'].'&status=success');
   
	}
}

add_key_for_playlist_api();








/*
* Creating a function to create our CPT
*/

