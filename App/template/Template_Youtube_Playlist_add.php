<style>
  #customers {
  
    border-collapse: collapse;
    width: 100%;
  }

  #customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: center;
  }
  #customers th 
  {
      font-family: fantasy;
      font-size: 18px;
      text-align: center;
  }

  #customers tr:nth-child(even){background-color: #FFFFFF;}
  #customers tr:nth-child(odd){background-color: #FFFFFF;}

  #customers tr:hover {background-color: #FFFFFF;}

  #customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    background-color: #007cba;
    color: white;
  }
  .container-forms
  {
      background-color: white;
      padding: 22px;
      border: 1px solid #eee;
      box-shadow: 0px 0px 3px 2px #EEF;
  }
  .label_of_table
  {
      font-size: 18px;
      margin-bottom: 10px;
      background-color: #F1F1F1;
      padding: 10px;
      border: 1px solid #eee;
  }
  .button_add
  {
      margin-top: 53px !important;
  }
  .sliding_loading , .sliding_loading_get
  {
      padding: 11px;
      border: 1px solid #eee;
      text-align: center;
      display: none;
  }
  .img_ajax_loading
  {
    width: 20px;
    height: 20px;
  }
  .heading_loading
  {
    font-size: 20px;
    font-family: cairo;
  }
   #customers td
  {
    font-size: 16px;
  }
  .success-message-youtube-playlist , .success-message-youtube-playlist-get-videos
  {
    display: none;
    text-align: center;
  }
  .success-message-youtube-playlist>.dashicons  , .success-message-youtube-playlist-get-videos>.dashicons
  {
    font-size: 46px;
    color: green;
    text-align: center;
  }

  .lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 8px;
  border-radius: 50%;
  border: 6px solid #007cba;
  border-color: #007cba transparent #007cba transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

</style> 
 <div class="container-forms"> 
      
    <form method="POST" action="#" class="form_ajax_get_playlist">
        <table style="width:100%" class="wp-list-table widefat fixed striped posts">
          <tr>
            <td style="width:40%">
                <div class="form-group place_input_admin">
                
                    <div class="label_of_table"><?php _e('رابط قائمة تشغيل يوتيوب','Youtube-Playlist') ?></div>
                
                    <input class="playlist_link" style="width:100%" type="text" value=""  class="form-control" name="course_hours" required/>
                   
                </div>
            </td>

            <td style="width:20%">
                <div class="form-group place_input_admin">
                    <div class="label_of_table"> <?php _e('اختار الكورس','Youtube-Playlist') ?> </div>
                	<?php
                       
                       $args = array(
                              'posts_per_page' => -1,
                              'post_type'   => 'stm-courses',
                            ); 
                       //var_dump(get_posts($args));
                    ?>
                    <select style="width:100%" class="form-control course_id"  name="course_depend_on" required>
                         <option value="">-select-</option>
                	<?php 

                        foreach(get_posts( $args ) as $courses){ ?>
                           <option value="<?php echo $courses->ID?>" ><?php echo $courses->post_title; ?></option>

                          
                           <?php  
                        }
                    ?>
                	
                    </select>
                	
                 
                </div>
      
            </td>
               <td style="width:20%">
                <div class="form-group place_input_admin">
                    <div class="label_of_table"> <?php _e('طريقة اضافة الدروس','Youtube-Playlist') ?> </div>
                 
                    <select style="width:100%" class="form-control type_of_get_lessons"  name="course_depend_on" required>
                         <option value="0"> <?php _e('اضافة الى محتوى الكورس','Youtube-Playlist') ?>  </option>
                         <option value="1"> <?php _e('افراغ الكورس و اضافة الدروس من جديد ','Youtube-Playlist') ?>  </option>
                    </select>
                  
                 
                </div>
      
            </td>

            <td style="width:20%">
                  <button type="submit" class="button button-primary button_add" > <?php _e('انشاء الدروس','Youtube-Playlist') ?>  </button>
                  
            </td>
          </tr>
        </table>
    </form>
    <div style="text-align:right;padding:10px">
        <button  class="button button-primary button_show_table"  > <?php _e('جلب الفيديوهات','Youtube-Playlist') ?>  </button>
    </div>
                  
            

    <div class="sliding_loading">
         <h4 class="heading_loading"> جاري الحصول على الفيديوهات  </h4>
        <!--  <img class="img_ajax_loading" src="<?php //echo EDR_PLUGIN_URL.'App/assets/img/ajax-loader.gif' ?> " /> -->
        <div class="lds-dual-ring"></div>
    </div>
     <div class="sliding_loading_get">
         <h4 class="heading_loading"> جاري جلب الفيديوهات  </h4>
        <!--  <img class="img_ajax_loading" src="<?php //echo EDR_PLUGIN_URL.'App/assets/img/ajax-loader.gif' ?> " /> -->
        <div class="lds-dual-ring"></div>
    </div>
    
    <div class="success-message-youtube-playlist">
        <span class="dashicons dashicons-yes"></span>
         <h4 class="heading_loading"> تم انشاء الدروس بنجاح  </h4>
    </div>
     <div class="success-message-youtube-playlist-get-videos">
        <span class="dashicons dashicons-yes"></span>
         <h4 class="heading_loading"> تم جلب الفيديوهات بنجاح  </h4>
    </div>

</div>
<br/>
<div style="background-color:white;padding: 15px;">
      <table style="width:100%" class="wp-list-table widefat fixed striped posts" id="customers">
        <thead>
          <tr>
            <th> <?php _e('رقم الفيديو','Youtube-Playlist'); ?> </th>
            <th> <?php _e('اسم الفيديو','Youtube-Playlist'); ?> </th>
            <th> <?php _e('رابط الفيديو ','Youtube-Playlist'); ?> </th>
            <th> <?php _e('مدة الفيديو ','Youtube-Playlist'); ?> </th>
          </tr>
        </thead>
        <tbody class="container-new-playlists">
            <?php
              /*$file_exist = file_exists(EDR_PLUGIN_DIR."playlist.txt");
              if($file_exist==1){
                  echo file_get_contents(EDR_PLUGIN_DIR."playlist.txt"); 
              }*/
            ?>
        </tbody>
          
    </table>

</div>
