<style>
  #customers {
  
    border-collapse: collapse;
    width: 100%;
  }

  #customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
    text-align: center;
  }
  #customers th 
  {
      font-family: fantasy;
      font-size: 18px;
      text-align: center;
  }

  #customers tr:nth-child(even){background-color: #FFFFFF;}
  #customers tr:nth-child(odd){background-color: #FFFFFF;}

  #customers tr:hover {background-color: #FFFFFF;}

  #customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    background-color: #007cba;
    color: white;
  }
  .container-forms
  {
      background-color: white;
      padding: 22px;
      border: 1px solid #eee;
      box-shadow: 0px 0px 3px 2px #EEF;
  }
  .label_of_table
  {
      font-size: 18px;
      margin-bottom: 10px;
      background-color: #F1F1F1;
      padding: 10px;
      border: 1px solid #eee;
  }
  .button_add
  {
      margin-top: 53px !important;
  }
  .sliding_loading
  {
      padding: 11px;
      border: 1px solid #eee;
      text-align: center;
      display: none;
  }
  .img_ajax_loading
  {
    width: 20px;
    height: 20px;
  }
  .heading_loading
  {
    font-size: 20px;
    font-family: cairo;
  }
   #customers td
  {
    font-size: 17px;
  }
  .success-message-youtube-playlist
  {
    display: none;
    text-align: center;
  }
  .success-message-youtube-playlist>.dashicons 
  {
    font-size: 46px;
    color: green;
    text-align: center;
  }

  .lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 8px;
  border-radius: 50%;
  border: 6px solid #007cba;
  border-color: #007cba transparent #007cba transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

</style> 
<?php 
if($_GET['status']=='success'){ ?>
    <div id="message" class="updated notice is-dismissible"><p class="  direction: rtl;text-align: left;"> <?php _e('تم اضافة api key  بنجاح','playlist-youtube'); ?> </p> </div>
  
<?php } ?>
 <div class="container-forms"> 
    <h2> <?php _e(' اعدادات YPG','Youtube-Playlist'); ?> </h2>
    <form method="POST" action="#" class="">
      <?php wp_nonce_field( 'wps-frontend-post' ); ?>
        <table style="width:100%" class="wp-list-table widefat fixed striped posts">
          <tr>
            <td style="width:40%">
                <div class="form-group place_input_admin">
                
                    <div class="label_of_table"><?php _e('Api Key الخاص بالاضافة','Youtube-Playlist') ?></div>
                
                    <input  style="width:100%" type="text" value="<?php echo ( !empty(get_option('playlist_key_youtube'))?get_option('playlist_key_youtube'):'null') ?>"  class="form-control" name="playlist_key" required/>
                   
                </div>
            </td>

            
            <td style="width:20%">
                  <button type="submit" class="button button-primary button_add add_key_api_youtube" name="add_key_api_youtube" > <?php _e('اضافة ','Youtube-Playlist') ?>  </button>
                  
            </td>
          </tr>
        </table>
    </form>
    
</div>
<br/>
