var all_data_back = '';
jQuery('.form_ajax_get_playlist').submit(function(e){
    e.preventDefault();
    
	var playlist_link=jQuery('.playlist_link').val();
	var course_id=jQuery('.course_id').val();
	var type_of_get_lessons =jQuery('.type_of_get_lessons').val();
	if(type_of_get_lessons==1){
	   var message_confirm = confirm('هل متأكد من انك تريد حذف الدروس ؟');
	}
	else if(type_of_get_lessons==0){
	   var message_confirm = confirm('هل متأكد اضافة الدروس الى ما يحتوية الكورس ؟ ');
	}
	if(message_confirm==true){
        jQuery('.button_add').attr('disabled',true);
		jQuery('.sliding_loading').slideDown('slow');
		jQuery.ajax({
		    type:'POST',
		    url:youtube_playlist_ajax.ajaxurl,
		    data:{
					action:'get_youtube_playlist',
					playlist_link:playlist_link,
					course_id:course_id,
					type_of_get_lessons:type_of_get_lessons,
		        },
			success:function(result){
				/*all_data_back = result['data'];*/
				if(result['getData']=='success'){

					jQuery('.sliding_loading').slideUp('slow',function(){
						jQuery('.success-message-youtube-playlist').slideDown('slow');
					});
	                
	                setInterval(function(){
	                   jQuery('.success-message-youtube-playlist').slideUp('slow');
	                },10000);
					jQuery('.button_add').attr('disabled',false);
				}
				
			}
		});
	}

});

jQuery('.button_show_table').click(function(e){
	var playlist_link=jQuery('.playlist_link').val();
	if(playlist_link!=''){
		jQuery('.success-message-youtube-playlist').slideUp('slow');
		e.preventDefault();
		jQuery('.button_show_table').attr('disabled',true);
		jQuery('.sliding_loading_get').slideDown('slow');

		jQuery.ajax({
		    type:'POST',
		    url:youtube_playlist_ajax.ajaxurl,
		    data:{
					action:'show_video_playlist_in_table',
					playlist_link:playlist_link,
		        },
			success:function(result){
					jQuery('.sliding_loading_get').slideUp('slow',function(){
				    	jQuery('.success-message-youtube-playlist-get-videos').slideDown('slow');
				    });
				 	setInterval(function(){
		                   jQuery('.success-message-youtube-playlist-get-videos').slideUp('slow');
		                },5000);
					jQuery('.container-new-playlists').html(result);
					jQuery('.button_show_table').attr('disabled',false);
			}
		});
	}
	else
	{
		alert('من فضلك قم بادخال رابط البلاى ليست لكى نقوم بجلب الفيديوهات');
	}
  
});

