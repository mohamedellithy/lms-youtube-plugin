<?php 

class wp_api_get_youtube_playlist{
    
     public $playlist_id;
	 public $youtube_api_key;
	 public $NextPage_token;
	 

	 function get_videos($link_attr){
        $parts = parse_url($link_attr);
	    $output = [];
	    parse_str($parts['query'], $output);
        $this->playlist_id = $output['list'];	
        $all_content=[];
        for($i=0;$i< $this->wp_api_get_youtube_playlist_times();$i++){
           $all_content[]= $this->get_playlist_video_items();

        }	

        return $all_content;
        
        
	 }

	 function wp_api_get_youtube_playlist_key(){
        $this->youtube_api_key = (!empty(get_option('playlist_key_youtube'))?get_option('playlist_key_youtube'):'AIzaSyCfDWuUvC9a7aIu08neyw0tVHH0UJzlVh0');
        return $this->youtube_api_key;
	 }

	 function wp_api_get_youtube_playlist_nextPage_token($token=null){
         return $this->NextPage_token=$token;
	 }

	 function wp_api_get_youtube_playlist_settings(){
	 	  /* here to get request api */
	 	   $requestUrl ='https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&pageToken='.$this->NextPage_token.'&maxResults=50&index=51&playlistId='.$this->playlist_id.'&key='.$this->wp_api_get_youtube_playlist_key();
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $requestUrl);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$all_video_youtube_response = curl_exec($ch);
			curl_close($ch);
			$all_video_youtube_json_response = json_decode( $all_video_youtube_response, TRUE );
		    return $all_video_youtube_json_response;
	 }

	 function wp_api_get_youtube_playlist_request_url(){
	 	   /* here to add nextpage token */
	 	   $setting_playlist_request = $this->wp_api_get_youtube_playlist_settings();
		   $this->wp_api_get_youtube_playlist_nextPage_token( (!empty($setting_playlist_request['nextPageToken'])?$setting_playlist_request['nextPageToken']:null) );
		   return $setting_playlist_request;
	 }

	 function wp_api_get_youtube_playlist_count_videos(){
	 	 /* here to get count video in playlist */
          $total_videos = $this->wp_api_get_youtube_playlist_settings();
          return $total_videos['pageInfo']['totalResults'];
	 }

	 function wp_api_get_youtube_playlist_times(){
	 	/* here to get times to call videos  */
	 	$count_times = $this->wp_api_get_youtube_playlist_count_videos()/50;
	 	return ceil($count_times);
	 }
	 function get_api_youtube_playlist_sizeVideo($video_id){
        //$all_times.= new DateTime( strtotime($data->items[0]->contentDetails->duration) ); // outputs 2:00 PM
           $requestUrl ='https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&id='.$video_id.'&key='.$this->wp_api_get_youtube_playlist_key();
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $requestUrl);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$all_video_youtube_response = curl_exec($ch);
			curl_close($ch);
			$video_youtube_json_response = json_decode($all_video_youtube_response);
		    return $video_youtube_json_response;
        
	 }

	 function set_date_settime($video_id){
         $size_of_video =  $this->get_api_youtube_playlist_sizeVideo($video_id);
	 	 $sizeVideo = $size_of_video->items[0]->contentDetails->duration;
	 	 $set_time = new DateInterval($sizeVideo);
	 	 $get_full_size = $set_time->h.':'.$set_time->i.':'.$set_time->s;
	 	 return $get_full_size;
	 }

	 

	 function get_playlist_video_items(){
	 	 $items_playlist = $this->wp_api_get_youtube_playlist_request_url();
	 	 $link_video=[];
	 	 foreach($items_playlist['items'] as $item ) {
            $videoID = $item['snippet']['resourceId']; 
	 	 	$videoTitle = $item['snippet']['title'];
	 	 	$size_of_video =  $this->set_date_settime($videoID['videoId']);
            $link_video[]=array('title'=>$item['snippet']['title'],
            	                'video_id'=>$videoID['videoId'],
            	                'position'=>$item['snippet']['position'],
            	                'thumbnails_url'=>$item['snippet']['thumbnails']['high']['url'],
            	                'size_video'=>$size_of_video,
            	          );
            
	 	 }

	 	 return $link_video;
	 }

}