<?php

class wp_api_get_add_playlist_video_to_post_lesson extends wp_api_get_youtube_playlist {
	public $all_videos_data;
	public $lesson_ids=array();
	public $course_id;
	static $video_embed_url = 'https://www.youtube.com/embed/';
	static $video_watch_url = 'https://www.youtube.com/watch?v='; 
	public $last_link_video;
	public $type_of_get_lessons;
	function __construct($link_video,$course_id=null,$type_of_get_lessons=null){
        $this->course_id = $course_id;
        $this->type_of_get_lessons = $type_of_get_lessons;
		if( empty($this->last_link_video) || ($this->last_link_video != $link_video) ){
		    $this->all_videos_data = $this->get_videos($link_video);
		  
		}
	}

	function add_lesson_for_course(){
		
		 $this->create_new_lesson();

	}

	function render_html_lessons_youtube(){
		$all_design_html = '';
        foreach ($this->all_videos_data as $data) {
        	foreach ($data as $list) {
                  $all_design_html.='<tr>
                                         <td>'.$list['position'].'</td>
                                         <td>'.$list['title'].'</td>
                                         <td><a target="_blank" href="'.wp_api_get_add_playlist_video_to_post_lesson::$video_watch_url.$list['video_id'].'" > مشاهدة الفيديو</a></td>
                                         <td>'.$list['size_video'].'</td>
                                     </tr>';
            }            
        }
        echo $all_design_html;
	}

	function create_new_lesson(){
		$all_posts =array(); 
		$old_content = $this->check_status_of_create_lessons();
        foreach ($this->all_videos_data as $data) {
        	foreach ($data as $list) {
                // Add the content of the form to $post as an array
	            $post = array(
	                'post_title'    => $list['title'],
	                /*'post_content'  => wp_api_get_add_playlist_video_to_post_lesson::$video_embed_url.$list['video_id'],*/
	                'post_status'   => 'publish',   // Could be: publish
	                'post_type'     => 'stm-lessons', // Could be: `page` or your CPT
	                'post_date'     => date("Y-m-d H:i:s"),
	                'post_date_gmt' => date("Y-m-d H:i:s"),
	                'menu_order'    => 1,
	            );
	            $post_id = wp_insert_post($post);
	            update_post_meta($post_id,'duration',$list['size_video']);
	            update_post_meta($post_id,'type','video');
	            $postr_link = $this->upload_postr_lesson($list['thumbnails_url'],$list['video_id']);
	            update_post_meta($post_id,'lesson_video_poster',$postr_link);
	            update_post_meta($post_id,'lesson_video_url',wp_api_get_add_playlist_video_to_post_lesson::$video_embed_url.$list['video_id']);
	            array_push($all_posts, $post_id);
            
            }            
        }
       
        $this->lesson_ids = $old_content.implode(',',$all_posts);
        update_post_meta($post_id,'duration',$list['size_video']);
        if(!empty($this->lesson_ids)){
        	
              update_post_meta($this->course_id,'curriculum',$this->lesson_ids);
			  header('Content-Type: application/json');
        	  echo json_encode(array('getData'=>'success'));
        	  //echo json_encode(array('getData'=>'success','data'=>$this->render_html_lessons_youtube()));
        }
        else
        {
        	header('Content-Type: application/json');
        	echo json(['getData'=>'failed']);
        }
	}




    function upload_postr_lesson($poster_link,$video_id){
		    
		    $image_url=$poster_link;

            $upload_dir = wp_upload_dir();

			$image_data = file_get_contents( $image_url );

			$filename = basename( $image_url );

			if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			  $file = $upload_dir['path'] . '/' . $video_id.$filename;
			}
			else {
			  $file = $upload_dir['basedir'] . '/' . $video_id.$filename;
			}

			file_put_contents( $file, $image_data );

			$wp_filetype = wp_check_filetype( $video_id.$filename, null );

			$attachment = array(
			  'post_mime_type' => $wp_filetype['type'],
			  'post_title' => sanitize_file_name( $video_id.$filename ),
			  'post_content' => '',
			  'post_status' => 'inherit'
			);

			$attach_id = wp_insert_attachment( $attachment, $file );
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			return $attach_id;
	}

	function check_status_of_create_lessons(){
		
		if($this->type_of_get_lessons==0){
          // here merge lessons
          $old_lessons = get_post_meta($this->course_id,'curriculum',true).',';
		}
		elseif($this->type_of_get_lessons==1){
		  // here delete old lessons
			$course_lessons_ids_get = get_post_meta($this->course_id,'curriculum',true);
		    $course_lessons_ids = explode(',', $course_lessons_ids_get);
		   foreach ($course_lessons_ids as $key) {
			   wp_delete_post($key,true);
           }
           delete_post_meta($this->course_id,'curriculum');
           $old_lessons = '';
		}

		return  $old_lessons;

	} 

	

}