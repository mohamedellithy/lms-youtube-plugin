<?php 

class wp_add_admin_pages_playlist_settings{
    
    public $MainPages=array();
    public $SubPages=array();

	function __construct(){
         
        $this->MainPages=[
                array(
               	  'Custom_Menu_Title' =>'YPG',
               	  'custom_menu'       =>'YPG',
               	  'capability'        =>'',
               	  'menu_slug'         =>'YPG',
               	  'call_function'     =>'',
               	  'icon_page'         =>'dashicons-welcome-learn-more',
               	  'order_in_menu'     =>10
               	)
		    ];

		    $this->SubPages=[
                array(
               	  'parent_slug'        =>'YPG',
               	  'page_title'         =>'Youtube-Playlist add',
               	  'menu_title'         =>'Youtube-Playlist add',
               	  'capability'         =>'manage_options',
               	  'menu_slug'          =>'playlist-add',
               	  'function'           =>array($this,'Template_Youtube_Playlist_add')
		               	
               	),
                array(
                  'parent_slug'        =>'YPG',
                  'page_title'         =>'Youtube-Playlist settings',
                  'menu_title'         =>'Youtube-Playlist settings',
                  'capability'         =>'manage_options',
                  'menu_slug'          =>'playlist-Settings',
                  'function'           =>array($this,'Template_Youtube_Playlist_settings')
                    
                ),
		    ];
            
          add_action('admin_menu',array($this,'add_main_pages'));
          add_action('admin_menu',array($this,'add_submain_pages'));

	}


	function add_main_pages(){
       foreach ($this->MainPages as $mainpage) {
			add_menu_page($mainpage['Custom_Menu_Title'],$mainpage['custom_menu'],
				$mainpage['capability'],$mainpage['menu_slug'],$mainpage['call_function'],
				$mainpage['icon_page'],$mainpage['order_in_menu']);
		}
	}

	function add_submain_pages(){
		foreach ($this->SubPages as $subpage) {
			add_submenu_page($subpage['parent_slug'],$subpage['page_title'],
				$subpage['menu_title'],$subpage['capability'],$subpage['menu_slug'],
				$subpage['function']);
		}
	}

	function Template_Youtube_Playlist_add(){
		require_once EDR_PLUGIN_DIR.'App/template/Template_Youtube_Playlist_add.php';
	}

  function Template_Youtube_Playlist_settings(){
    require_once EDR_PLUGIN_DIR.'App/template/Template_Youtube_Playlist_settings.php';
  }


}