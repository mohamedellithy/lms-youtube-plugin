<?php 

/**
 * Plugin Name:  YouTube playlist generator YPG
 * Plugin URI: http://YouTubeplaylistgeneratorYPG.com/
 * Description:Youtube Playlist for adding youtube playlist to courses
 * Author: mohamed ellithy
 * Author URI: https://mostaql.com/u/mohamedeloms
 * Version: 2.0.3
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Youtube-Playlist
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'EDR_VERSION', '2.0.3' );
define( 'EDR_DB_VERSION', '2.0' );
define( 'EDR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'EDR_PLUGIN_URL', plugin_dir_url( __FILE__ ) );


require_once EDR_PLUGIN_DIR.'/App/function.php';